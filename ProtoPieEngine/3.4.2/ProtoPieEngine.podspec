Pod::Spec.new do |s|  
    s.name              = 'ProtoPieEngine'
    s.version           = '3.4.2'
    s.summary           = 'ProtoPie iOS Engine'
    s.homepage          = 'https://www.protopie.io'
    s.author            = 'Stuido XID'
    s.license           = 'Proprietry'
    s.platform          = :ios
    s.source            = { :http => 'http://localhost:8080/ProtoPieEngine.zip' }
    s.ios.deployment_target = '8.0'
    s.ios.vendored_frameworks = 'ProtoPieEngine.framework'
end
