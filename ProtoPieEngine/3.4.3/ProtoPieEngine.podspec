Pod::Spec.new do |s|  
    s.name              = 'ProtoPieEngine'
    s.version           = '3.4.3'
    s.summary           = 'ProtoPie iOS Engine'
    s.homepage          = 'https://www.protopie.io'
    s.author            = 'Stuido XID'
    s.license           = { :type => 'Proprietry', :text => 'Copyright 2017 Studio XID, Inc' }
    s.platform          = :ios
    s.source            = { :http => 'https://s3.ap-northeast-2.amazonaws.com/protopie-sdk/ProtoPieEngine-3.4.3.zip' }
    s.ios.deployment_target = '8.0'
    s.ios.vendored_frameworks = 'ProtoPieEngine.framework'
end
